module.exports = function(RED) 
{
    function GetValue(config) 
    {  
        RED.nodes.createNode(this, config);
        var node = this;

        let ip = config.ip;
        let portStr = "/iolinkmaster/port[" + config.port + "]/iolinkdevice/pdin/getdata";

        let requestObj = {"code": "request", "cid": 4, "adr": portStr};

        let requestJSON = JSON.stringify(requestObj);

        const https = require('http');
        
        if(config.refreshRate < 0.01)
            config.refreshRate = 0.01;

        setInterval(function()
        {    
            let options = 
            {
                hostname: ip,
                port: 80,
                method: 'POST',
                headers:
                {
                    'Content-Type': 'application/json',
                    'Content-Length': requestJSON.length,
                }
            }

            let request = https.request(options, function(result)
            {
                result.on('data', function(data)
                {
                    try
                    {
                        var newJson = JSON.parse(data);

                        let msg = {};
                        msg.payload = 
                        {
                            rawValue: newJson.data.value,
                            hex: newJson.data.value.substring(0, 4),
                            dec: (parseInt(newJson.data.value, 16) >> 16) / 10,
                            str: ((parseInt(newJson.data.value, 16) >> 16) / 10) + "°C"
                        }
                        node.send(msg);
                    }
                    catch(e)
                    {
                        console.log(e);
                    }
                })
            });

            request.on('error', function(error) {
                //avoid ECONNRESET due to Node-Red bug
              })

            request.write(requestJSON);
            request.end();
        }, config.refreshrate * 1000);

        node.on('input', function(msg) 
        {
            
        });
    }

    RED.nodes.registerType("iolink-TV7105", GetValue);
}