module.exports = function(RED) 
{
    function GetValue(config) 
    {  
        RED.nodes.createNode(this, config);
        var node = this;

        let ip = config.ip;
        let portStr = "/iolinkmaster/port[" + config.port + "]/iolinkdevice/pdin/getdata";

        let requestObj = {"code": "request", "cid": 4, "adr": portStr};

        let requestJSON = JSON.stringify(requestObj);

        const https = require('http');
        
        if(config.refreshRate < 0.01)
            config.refreshRate = 0.01;

        setInterval(function()
        {    
            let options = 
            {
                hostname: ip,
                port: 80,
                method: 'POST',
                headers:
                {
                    'Content-Type': 'application/json',
                    'Content-Length': requestJSON.length,
                }
            }

            let request = https.request(options, function(result)
            {
                result.on('data', function(rdata)
                {
                    try
                    {
                        var original = JSON.parse(rdata).data.value;
                        var raw = original;
                        var statusHex = "";

                        var split = [];

                        var i = 0;
                        while(original.length > 0)
                        {
                            statusHex = original[2];
                            
                            var hex = original.slice(0, 4);
                            var splitObj = {hex:hex, dec:parseInt(hex, 16)};
                            
                            original = original.slice(4, original.length)
                            
                            if(i % 2 === 0)
                            {
                                splitObj.dec>>8;
                            }
                            
                            split.push(splitObj);
                        }

                        let data =
                        {
                            status: {hex: statusHex}
                        }

                        var vals = split;

                        var labeled = {};

                        labeled.v_Rms = vals[0];
                        labeled.Scale_v_Rms = vals[1];
                        labeled.a_Peak = vals[2];
                        labeled.Scale_a_Peak = vals[3];
                        labeled.a_Rms = vals[4];
                        labeled.Scale_a_Rms = vals[5];
                        labeled.Temperature = vals[6];
                        labeled.Scale_Temperature = vals[7];
                        labeled.Crest = vals[8];
                        labeled.Scale_Crest = vals[9];

                        labeled.Temperature.dec *= 0.1;
                        labeled.v_Rms.dec *= 0.0001;
                        labeled.a_Rms.dec *= 0.1;
                        labeled.Crest.dec *= 0.1;
                        labeled.a_Peak.dec *= 0.1;

                        data.values = labeled;

                        var status = "Catastrophic Failure"

                        switch(data.status.hex)
                        {
                            case "0":
                                status = "OK";
                                break;
                            case "1":
                                status = "Maintainance Required";
                                break;
                            case "2":
                                status = "Out of Specification";
                                break;
                            case "3":
                                status = "Functional Check";
                                break;
                            case "4":
                                status = "Failure";
                                break;
                        }

                        data.status.str = status;

                        let msg = {};
                        msg.payload = data;
                        node.send(msg);
                    }
                    catch(e)
                    {
                        console.log(e);
                    }
                })
            });

            request.on('error', function(error) {
                //avoid ECONNRESET due to Node-Red bug
              })

            request.write(requestJSON);
            request.end();
        }, config.refreshrate * 1000);

        node.on('input', function(msg) 
        {
            
        });
    }

    RED.nodes.registerType("iolink-VVB010", GetValue);
}