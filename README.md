A collection of various sensors designed for IFM's IOLink Master for use in Node-RED. May work for other IOLink Master devices, but has only been tested on an IFM master.

Each node has parameters for the Master IP and the IOLink port, as well as a refresh rate in seconds. Data will be output as-is.

I am making this package as I learn more about the IOLink sensors, as such this package will be updated over time with new sensors.